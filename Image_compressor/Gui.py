import tkinter as tk
import numpy as np
import cv2
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
import matplotlib.cm as cm
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from scipy.fftpack import dct
from scipy.fftpack import idct
from PIL import ImageTk, Image
import os


#Definizioni
def esplora():
    global filename
    filename = filedialog.askopenfile(parent=root, title="Select file", filetypes=[("Bitmap image", "*.bmp")])
    filename = filename.name
    if filename != None and filename[len(filename)-4:len(filename)]==".bmp":
        show_image1()
    else:
        messagebox.showinfo("Error Message", "Per proseguire è necessario selezionare un file in formato bitmap")


def get_dct(img):
    return dct(dct(img.T, norm='ortho').T, norm='ortho')

def get_idct(coefficients):
    return idct(idct(coefficients.T, norm='ortho').T, norm='ortho')   

def new_image():
    global F, d, blocksH, blocksV
    img1=cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    img1 = np.array(img1, dtype=np.float)
    h,w = np.array(img1.shape[:2])/F * F
    h = int(h)
    w = int(w)
    img1=img1[:h,:w]
    blocksV=int(h/F)
    blocksH=int(w/F)
    vis0 = np.zeros((h,w), np.float32)
    Trans = np.zeros((h,w), np.float32)
    vis0[:h, :w] = img1
    for row in range(blocksV): #ciclo immagine in larghezza
	    for col in range(blocksH): #ciclo immagine in altezza
		    currentblock = get_dct(vis0[row*F:(row+1)*F,col*F:(col+1)*F]) #prendo il blocco fatto da BxB pixel
		    for k in range(currentblock.shape[0]): #ciclo il blocco in altezza
			    for l in range(currentblock.shape[1]): #ciclo il blocco in larghezza
				    if k+l>=d:
					    currentblock[k,l]=0		
		    Trans[row*F:(row+1)*F,col*F:(col+1)*F]=currentblock
    cv2.imwrite('Transformed.jpg', Trans)

    back0 = np.zeros((h,w), np.float32)
    for row in range(blocksV):
	    for col in range(blocksH):
		    back0[row*F:(row+1)*F,col*F:(col+1)*F] = get_idct(Trans[row*F:(row+1)*F,col*F:(col+1)*F])


    for k in range(back0.shape[0]): #ciclo il blocco in altezza
	    for l in range(back0.shape[1]): #ciclo il blocco in larghezza
		    if back0[k,l]<=0:
			    back0[k,l]=0
		    else:	
			    if back0[k,l]>=255:
				    back0[k,l]=255
			    else:
				    if not isinstance(back0[k,l], int):
					    back0[k,l]=int(round(back0[k,l]))
    cv2.imwrite('ImmagineConvertita.jpg', back0)
    show_image2()


def set_variables():
    choose_F_label=tk.Label(frame_input, text="Scegli F:", font='Arial 20', bg='#595959', fg='#ffffff')
    choose_F_label.place(x=600, y=75)
    choose_F_entry=tk.Entry(frame_input, bd=5, textvariable=F, width=5)
    choose_F_entry.place(x=720, y=80)
    button_ok_image = tk.Button(frame_input, image=search_ok_icon, bg='white', command=set_F)
    button_ok_image.place(x=800, y=70)
   
def set_F():
    global F
    F = F.get()
    choose_d()

def choose_d():
    txt='Soglia di taglio min('+ str(0)+') - max('+ str(2*F-2)+'):'
    choose_d_label=tk.Label(frame_input, text=txt, font='Arial 20', bg='#595959', fg='#ffffff')
    choose_d_label.place(x=1000, y=75)
    #d_range_value= tk.Scale(frame_input, from_=0, to=(2*F-2), variable=F, orient=HORIZONTAL)
    d_range_value=tk.Entry(frame_input, bd=5, textvariable=d )
    d_range_value.place(x=1425, y=80)
    button_ok_d = tk.Button(frame_input, image=search_ok_icon, bg='white', command=set_d)
    button_ok_d.place(x=1570, y=70)

def set_d():
    global d, F
    d= d.get()
    if d >=((2*F-2)+1):
        messagebox.showinfo("Error Message", "Per proseguire bisogna inserire un valore compreso tra " +str(0)+ " e " +str(2*F-2))
    new_image()
    
    
def show_image1():
    global filename
    img1 = Image.open(filename)
    N, M = img1.size
    txt = 'Originale (Dimensioni: ' + str(N) + 'x' + str(M) +')'
    original_img_label=tk.Label(root, text=txt, font='Arial 20', bg='#737373', fg='#ffffff')
    original_img_label.place(x=250,y=320)
    img = img1.resize((825, 550), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel = Label(root, image=img)
    panel.image = img
    panel.place(x=220, y=390)
    dimension_img=os.path.getsize(filename)
    txt1 = 'Spazio occupato su disco: '+ str(round(dimension_img/(1024*1024),2)) +' MB'
    original_img_dimension_label=tk.Label(root, text=txt1, font='Arial 20', bg='#737373', fg='#ffffff')
    original_img_dimension_label.place(x=250,y=955)
    set_variables()

def show_image2():
    global F, blocksH, blocksV
    img2 = Image.open("ImmagineConvertita.jpg")
    area=(0,0,blocksH*F,blocksV*F)
    cropped=img2.crop(area)
    txt = 'Convertita (Dimensioni: ' + str(blocksH*F) + 'x' + str(blocksV*F) +')'
    transformed_img_label=tk.Label(root, text=txt, font='Arial 20', bg='#737373', fg='#ffffff')
    transformed_img_label.place(x=1200,y=320)
    img = cropped.resize((825, 550), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel = Label(root, image=img)
    panel.image = img
    panel.place(x=1050, y=390)
    dimension_img=os.path.getsize("ImmagineConvertita.jpg")
    txt1 = 'Spazio occupato su disco: '+ str(round(dimension_img/(1024*1024),2)) +' MB'
    transformed_img_dimension_label=tk.Label(root, text=txt1, font='Arial 20', bg='#737373', fg='#ffffff')
    transformed_img_dimension_label.place(x=1200,y=955)
    set_variables()




#Interfaccia Grafica
HEIGHT =2500
WIDTH =2000

root= tk.Tk()

name = StringVar()
F = IntVar()
d = IntVar()
canvas=tk.Canvas(root, height=HEIGHT, width=WIDTH)
canvas.pack()
search_ok_icon=tk.PhotoImage(file='./immagini/check.png')
background_label=tk.Label(root, bg='#737373')
background_label.place( relwidth=1, relheight=1)

frame_sinistra3=tk.Frame(root, bg='#737373', width=230, height=2500)
frame_sinistra3.place(x=0, y=0)

frame_input=tk.Frame(root, bg='#595959', width=2500, height=150)
frame_input.place(x=0, y=130)

choose_image_label=tk.Label(frame_input, text="Apri Immagine:", font='Arial 20', bg='#595959', fg='#ffffff')
choose_image_label.place(x=230, y=75)
search_icon=tk.PhotoImage(file='./immagini/search_icon.png')
button_search_image = tk.Button(frame_input, image=search_icon, command=esplora)
button_search_image.place(x=440, y=70)


frame_sinistra2=tk.Frame(root, bg='#595959', width=220, height=2500)
frame_sinistra2.place(x=0, y=0)

frame_sinistra1=tk.Frame(root, bg='#404040', width=210, height=2500)
frame_sinistra1.place(x=0, y=0)

frame_sinistra=tk.Frame(root, bg='#0d0d0d', width=200, height=2500)
frame_sinistra.place(x=0, y=0)

frame_titolo=tk.Frame(root, bg='#0d0d0d', width=2500, height=150)
frame_titolo.place(x=0, y=0)
titolo=tk.Label(frame_titolo, text="Compressione di immagini tramite la DCT", font='Arial 50', bg='#0d0d0d', fg='#ffffff')
titolo.place(x=400, y=50)

nomi_label1=tk.Label(frame_sinistra, text="Andrea\nAgostinacchio", font='Arial 20', bg='#0d0d0d', justify='left', fg='#ffffff')
nomi_label1.place(x=10, y=400)
nomi_label2=tk.Label(frame_sinistra, text="Leonardo\nGiannoni", font='Arial 20', bg='#0d0d0d', justify='left', fg='#ffffff')
nomi_label2.place(x=10, y=500)
nomi_label3=tk.Label(frame_sinistra, text="Andrea\nNapoleoni", font='Arial 20', bg='#0d0d0d', justify='left', fg='#ffffff')
nomi_label3.place(x=10, y=600)

root.mainloop()